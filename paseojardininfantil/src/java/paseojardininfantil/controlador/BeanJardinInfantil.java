/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paseojardininfantil.controlador;

import jardininfantillistase.controlador.ListaSE;
import jardininfantillistase.modelo.Infante;
import jardininfantillistase.modelo.Nodo;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.connector.StateMachineConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;
import paseojardininfantil.controlador.utilidades.JsfUtil;

/**
 *
 * @author carloaiza
 */
@Named(value = "beanJardinInfantil")
@SessionScoped
public class BeanJardinInfantil implements Serializable {

    private boolean deshabilitarNuevo = true;

    private ListaSE listaSE = new ListaSE();

    private Nodo infanteMostrar = new Nodo(new Infante());

    private boolean verTabla = false;

    private DefaultDiagramModel model;

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public boolean isVerTabla() {
        return verTabla;
    }

    public void setVerTabla(boolean verTabla) {
        this.verTabla = verTabla;
    }

    /**
     * Creates a new instance of BeanJardinInfantil
     */
    public BeanJardinInfantil() {

    }

    @PostConstruct
    public void llenarInfantes() {
        listaSE.adicionarNodo(new Infante("Martín", (byte) 3, 'M'));
        listaSE.adicionarNodo(new Infante("Estefanía", (byte) 2, 'F'));
        listaSE.adicionarNodo(new Infante("Sebastián", (byte) 4, 'M'));
        irAlPrimero();
        pintarLista();
    }

    public boolean isDeshabilitarNuevo() {
        return deshabilitarNuevo;
    }

    public void setDeshabilitarNuevo(boolean deshabilitarNuevo) {
        this.deshabilitarNuevo = deshabilitarNuevo;
    }

    public ListaSE getListaSE() {
        return listaSE;
    }

    public void setListaSE(ListaSE listaSE) {
        this.listaSE = listaSE;
    }

    public Nodo getInfanteMostrar() {
        return infanteMostrar;
    }

    public void setInfanteMostrar(Nodo infanteMostrar) {
        this.infanteMostrar = infanteMostrar;
    }

    public void habilitarCrearInfante() {
        deshabilitarNuevo = false;
        infanteMostrar = new Nodo(new Infante());
    }

    public void guardarInfante() {
        listaSE.adicionarNodo(infanteMostrar.getDato());
        infanteMostrar = new Nodo(new Infante());
        deshabilitarNuevo = true;
        irAlPrimero();
        JsfUtil.addSuccessMessage("Se ha adicionado con éxito");
    }

    public void guardarInfanteAlInicio() {
        listaSE.adicionarNodoAlInicio(infanteMostrar.getDato());
        infanteMostrar = new Nodo(new Infante());
        deshabilitarNuevo = true;
        irAlPrimero();

    }

    public void irAlPrimero() {
        infanteMostrar = listaSE.getCabeza();
    }

    public void irAlSiguiente() {
        if (infanteMostrar.getSiguiente() != null) {
            infanteMostrar = infanteMostrar.getSiguiente();
        }
    }

    public void irAlUltimo() {

    }

    public void invertirLista() {
        listaSE.invertirLista();
        irAlPrimero();
        pintarLista();
    }

    public void cancelarGuardado() {
        deshabilitarNuevo = true;
        irAlPrimero();
    }

    public void visualizarTabla() {
        verTabla = true;
    }

    public void eliminarInfante() {
        listaSE.eliminarNodo(infanteMostrar.getDato());
        ///Mostrar mensaje
        irAlPrimero();
    }

    public void pintarLista() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);

        StateMachineConnector connector = new StateMachineConnector();
        connector.setOrientation(StateMachineConnector.Orientation.ANTICLOCKWISE);
        connector.setPaintStyle("{strokeStyle:'#7D7463',lineWidth:3}");
        model.setDefaultConnector(connector);

        ///Adicionar los elementos
        if (listaSE.getCabeza() != null) {
            Nodo temp = listaSE.getCabeza();
            int posX=5;
            int posY=2;
            while(temp !=null)
            {
                //Parado en un elemento
                Element ele = new Element(temp.getDato().getNombre(), posX+"em", posY+"em");
                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
                ele.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_RIGHT));
                model.addElement(ele);                    
                temp=temp.getSiguiente();
                posX=  posX+15;
                posY= posY+10;
            }
            
            
            for(int i=0; i < model.getElements().size() -1; i++)
            {
                model.connect(createConnection(model.getElements().get(i).getEndPoints().get(1), 
                        model.getElements().get(i+1).getEndPoints().get(0), "Sig"));
            }

            
        }
    }

    private Connection createConnection(EndPoint from, EndPoint to, String label) {
        Connection conn = new Connection(from, to);
        conn.getOverlays().add(new ArrowOverlay(20, 20, 1, 1));

        if (label != null) {
            conn.getOverlays().add(new LabelOverlay(label, "flow-label", 0.5));
        }

        return conn;
    }

}
